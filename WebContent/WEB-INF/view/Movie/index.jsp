<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Movie"%>
<%@ include file = "../header.jsp" %>

<%! List <Movie> movies;%>

<%
  movies = (List<Movie>)request.getAttribute("movies");
  if (movies == null) {
	  movies = new ArrayList<Movie>();
  }
%>
<main>
	<h1>Lista de pel�culas</h1>
	<table>
	<tr>
		<th>T�tulo</th>
		<th>Director</th>
		<th>A�o</th>
		<th>Operaciones</th>
	</tr>
	<% for (Movie item : movies) {%>
	  <tr>
	  <td><%= item.getTitle() %></td>
	  <td><%= item.getDirector() %></td>
	  <td><%= item.getYear() %></td>
	  <td>
	  	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/remember">recordar</a>
	  	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/delete">borrar</a>
	  </td>
	  </tr>
	<% } %>
	</table>
</main>
<%@ include file = "../footer.jsp" %>