package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Movie;
import persistence.MovieDAO;

/**
 * Servlet implementation class MovieServlet
 */
@WebServlet(urlPatterns = { "/movies", "/movies/*" })
public class MovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MovieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String[] pieces = getUriPieces(request);

        int length = pieces.length;
        if(length == 3){
        	//index
            index(request, response);
        }else if (length > 3) {
            String piece = pieces[3];
            int id = 0;
       
            if("index".equals(piece)){
                index(request, response);            	
            }else if("create".equals(piece)){
            	create(request, response);
            }
            if(length == 5){
                try {
                    id = Integer.parseInt(piece);
                } catch (Exception e) {
                    response.sendRedirect(request.getContextPath());
                    return;
                }
            	System.out.println("longitud 5");
        		String piece4 = pieces[4];
        		if("remember".equals(piece4)){
        			remember(request, response, id);
        		}else if("delete".equals(piece4)){
        			delete(request, response, id);
        		}
			}
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] pieces = getUriPieces(request);
        int length = pieces.length;
        if (length == 3) {
            System.out.println("longitud 3");
            store(request, response);
        }if(length > 3){
        	System.out.println("longitud 4");
        }
		//doGet(request, response);
	}
	
	private void index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/Movie/index.jsp");
		ArrayList<Movie> movies= null;
        try {
            MovieDAO dao = new MovieDAO();
            movies = dao.all();
        } catch (SQLException ex) {
            // TODO: handle exception
            System.out.println("Fallo en articles.all()");
            System.out.println("SQLException: " + ex.getMessage());
        }
        
        request.setAttribute("movies", movies);
        dispatcher.forward(request, response);
	}
	
	private void create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/Movie/create.jsp");
		dispatcher.forward(request, response);
	}
	
	private void store(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        Movie movie = new Movie();

        movie.setTitle(request.getParameter("title"));
        movie.setDirector(request.getParameter("director"));
        try {
            movie.setYear(Integer.parseInt(request.getParameter("year")));
        } catch (Exception e) {
            movie.setYear(0);
        }
        
        try {
            MovieDAO dao = new MovieDAO();
            dao.store(movie);
        } catch (SQLException ex) {
            // TODO: handle exception
            System.out.println("Fallo en articles.all()");
            System.out.println("SQLException: " + ex.getMessage());
        }

        response.sendRedirect(request.getContextPath() + "/movies/index");
	}
	
	private void remember(HttpServletRequest request, HttpServletResponse response, int id) throws ServletException, IOException{
		Movie movie = new Movie();
		MovieDAO dao = new MovieDAO();
		try{
			movie = dao.getById(id);
		}catch(SQLException ex){
			// TODO: handle exception
            System.out.println("Fallo en movies.all()");
            System.out.println("SQLException: " + ex.getMessage());
		}
        
		ArrayList <Movie> moviesToRemember;
        HttpSession session = request.getSession(true);
        if (session.getAttribute("moviesToRemember") == null) {
            session.setAttribute("moviesToRemember", new ArrayList<Movie>());
        }
        
    	moviesToRemember = (ArrayList<Movie>)session.getAttribute("moviesToRemember");
    	moviesToRemember.add(movie); 
        response.sendRedirect(request.getContextPath() + "/movies/index");
	}
	
	private void delete(HttpServletRequest request, HttpServletResponse response, int id) throws ServletException, IOException{
		MovieDAO dao = new MovieDAO();
		try{
			dao.delete(id);
		}catch(SQLException ex){
			// TODO: handle exception
            System.out.println("Fallo en articles.all()");
            System.out.println("SQLException: " + ex.getMessage());
		}
        response.sendRedirect(request.getContextPath() + "/movies/index");
	}
	
    private String[] getUriPieces(HttpServletRequest request) {
        String uri = request.getRequestURI();
        String[] pieces = uri.split("/");
        return pieces;
    }

}
